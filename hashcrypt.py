#!/bin/python
#-*- coding: utf-8 -*-
#Contáctame :https://wa.me/50498934262

#librerías
import hashlib
import time
from os import system
from pyfiglet import figlet_format

#banner
def banner():
	system('clear')
	print ('\033[1;36m',figlet_format('Hash',font='small'))
	print ('\033[1;31m',figlet_format('Crypt',font='small'))
	time.sleep(1.5)
	print ('\n\033[1;30mCreated by: \033[1;36mF@br1x\n')
banner()


palabra = input("\033[1;37mIngrese el texto a cifrar:\033[1;33m ")
time.sleep(2)

#Proceso del cifrado del texto
contra_md5 = hashlib.md5(palabra.encode()).hexdigest()
contra_sha1 = hashlib.sha1(palabra.encode()).hexdigest()
contra_sha224 = hashlib.sha224(palabra.encode()).hexdigest()
contra_sha256 = hashlib.sha256(palabra.encode()).hexdigest()
contra_sha512 = hashlib.sha512(palabra.encode()).hexdigest()
print ()

#Resultado final de los cifrados :D
print (f"\033[1;36mMD5: \033[1;32m{contra_md5}")
time.sleep(0.5)
print (f"\033[1;36mSHA1: \033[1;32m{contra_sha1}")
time.sleep(0.5)
print (f"\033[1;36mSHA224: \033[1;32m{contra_sha224}")
time.sleep(0.5)
print (f"\033[1;36mSHA256: \033[1;32m{contra_sha256}")
time.sleep(0.5)
print (f"\033[1;36mSHA512: \033[1;32m{contra_sha512}")
time.sleep(0.5)

#Fin del proceso :3
print ("\n\033[1;33mTodos los cifrados mostrados correctamente :)")
time.sleep(3)
